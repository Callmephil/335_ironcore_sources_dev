DELETE FROM `trinity_string` WHERE `entry` IN (13000, 13001);
INSERT INTO `trinity_string` VALUES ('13000', 'Guild Level: %u.', null, null, null, null, null, null, null, null);
INSERT INTO `trinity_string` VALUES ('13001', 'Guild Experience: %u/%u.', null, null, null, null, null, null, null, null);
DELETE FROM `command` WHERE `name` IN ('guild linfo', 'guild setlevel', 'guild givexp');
INSERT INTO `command` VALUES ('guild linfo', '2', 'Syntax: .guild linfo');
INSERT INTO `command` VALUES ('guild setlevel', '2', 'Syntax: .guild setlevel $GuildName $Level');
INSERT INTO `command` VALUES ('guild givexp', '2', 'Syntax: .guild givexp $GuildName $Xp');