/*
Wiki : 
IronMan State :
1 = Active
2 = Yellow Flag
3 = Red Flag
4 = Honoured Dead
5 = Discalified
*/

#ifndef EXTENSION_H_
#define EXTENSION_H_

class Player;
class Unit;
class GameObject;
class Creature;
class ZoneScript;
struct GossipMenuItems;

#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptedEscortAI.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "World.h"
#include "PassiveAI.h"
#include "GameEventMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Cell.h"
#include "CellImpl.h"
#include "SpellAuras.h"
#include "Pet.h"
#include "PetAI.h"
#include "CreatureTextMgr.h"
#include "SmartAI.h"

enum General_Menu
{
	MAIN_MENU,
	M_CONFIRM
}; 

enum Cha_IronMan_Menu
{
	QUEUE_TO_CHALLENGE = 10,
	PARTICIPANT_LIST,
	YELLOW_FLAGGED_LIST,
	RED_FLAGGED_LIST,
	HONOURED_DEAD_LIST
};

enum Cha_Leveling_Menu
{
	INFO_NORMAL_MODE = 10,
	INFO_MEDIUM_MODE,
	INFO_HARD_MODE,
	INFO_TORMENT_MODE,
	Q_NORMAL_MODE,
	Q_MEDIUM_MODE,
	Q_HARD_MODE,
	Q_TORMENT_MODE
};

/* Extra Icons */
#define DEAD_ICON				  "|TInterface\\ICONS\\inv_misc_bone_humanskull_02:20:20:0:0|t "
// #define M_ICO_BACK				  "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:30:30:-20:0|t"



/* Menu Icons */
#define M_ICO_QUEUE				  "|TInterface\\ICONS\\achievement_bg_killxenemies_generalsroom:30:30:-20:0|t"
#define M_ICO_ACTIVE_LIST		  "|TInterface\\ICONS\\achievement_bg_killflagcarriers_grabflag_capit:30:30:-20:0|t"
#define M_ICO_RED_FLAG			  "|TInterface\\ICONS\\inv_banner_03:30:30:-20:0|t"
#define M_ICO_YELLOW_FLAG		  "|TInterface\\ICONS\\spell_misc_warsongbrutal:30:30:-20:0|t"
#define M_ICO_DEAD_ICON			  "|TInterface\\ICONS\\achievement_bg_kill_on_mount:30:30:-20:0|t"
// Cha_Leveling 
#define M_ICO_NORMAL			  "|TInterface\\ICONS\\ability_creature_cursed_01:30:30:-20:0|t"
#define M_ICO_MEDIUM			  "|TInterface\\ICONS\\ability_creature_cursed_02:30:30:-20:0|t"
#define M_ICO_HARD				  "|TInterface\\ICONS\\ability_creature_cursed_03:30:30:-20:0|t"
#define M_ICO_TORMENT			  "|TInterface\\ICONS\\ability_creature_cursed_04:30:30:-20:0|t"

/* General */
#define M_TXT_BACK				  "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:30:30:-20:0|t[Return]"
#define M_TXT_NORESULT			  "[No Result]"
#define M_TEXT_CONFIRM			  "[Confirm]"

/* Specific for Iron Man Challenge NPC */  
#define M_TXT_QUEUE_TO_CHALLENGE  "[Queue to Iron Man Challenge]"
#define M_TXT_PARTICIPANT_LIST 	  "[Active Participant List]"
#define M_TXT_YELLOW_FLAGGED_LIST "[Yellow Flagged List]"
#define M_TXT_RED_FLAGGED_LIST 	  "[Red Flagged List]"
#define M_TXT_HONOURED_DEAD_LIST  "[Honoured Dead List]"

/* Specific for Leveling Challenge NPC */
#define M_TXT_QUEUE_TO_NORMAL_MODE  "[Normal Mode]"
#define M_TXT_QUEUE_TO_MEDIUM_MODE  "[Medium Mode]"
#define M_TXT_QUEUE_TO_HARD_MODE    "[Hard   Mode]"
#define M_TXT_QUEUE_TO_TORMENT_MODE "[Torment I]"

class Extension_Challenges
{
    private:
		Extension_Challenges() { };
		~Extension_Challenges() { };

    public:
		static Extension_Challenges* instance()
        {
			static Extension_Challenges instance;
            return &instance;
        }
		
	public: // IronMan Challenges
		/* IronMan Challenge NPC */
		bool Queue_Or_Show_Participation(Player* player, Creature* creature);
		bool Show_Participant_List(Player* player, Creature* creature);
		bool Show_Yellow_Flag_List(Player* player, Creature* creature);
		bool Show_Red_Flag_List(Player* player, Creature* creature);
		bool Show_Honoured_Dead_List(Player* player, Creature* creature);
		
		/* IronMan Challenge Checks */ 
		bool Challenge_Restriction(Player* player = NULL);
		bool IsIronMan() const;
		bool IsIronMan(Player* player) const;
		void Queue_IronMan_Challenge(Player* player);
		void Set_Red_Flag(Player* player);
		void Set_Yellow_Flag(Player* player);
		void Set_Honoured_Dead(Player* player);
		void Set_Discalified(Player* player);
		
	private:
	
	public: // Leveling Challenges
		/* Leveling Challenge NPC */
		void Queue_Normal_Mode(Player* player);
		void Queue_Medium_Mode(Player* player);
		void Queue_Hard_Mode(Player* player);
		void Queue_Torment_Mode(Player* player);
		/* Leveling Challenge Checks */
		
		
	private:
	
};

#define sExtensionChallenges Extension_Challenges::instance()

#endif