/*
Original Script By : Callmephil
Current Supported Version : SunWellCore/IronCore 3.3.5 (2009)
Original Idea : PaTiiCoS | Mathias (Skype)
Script Progression : 10 %
*/

/*

*/

#include "ScriptPCH.h"
#include "Config.h"
#include "Chat.h"
#include "Language.h"
#include "Guild.h"

void LoadGuildLevelInfo(Player* player)
{
	uint16 GuildLevel = player->GetGuild()->GetLevel();
	std::stringstream GuildInfo1;
	std::stringstream GuildInfo2;


	if (player->GetGuild())
	{
		if (player->GetGuild()->GetLevel() >= 8)
		{
			GuildInfo1 << "[Guild Level] : " << GuildLevel;
			GuildInfo2 << "[Guild Experience] : " << "Max/Max" << "|r";
		}
		else
		{
			GuildInfo1 << "[Guild Level] : " << GuildLevel;
			GuildInfo2 << "[Guild Experience] : " << player->GetGuild()->GetCurrentXP() << "/" << player->GetGuild()->GetXpForNextLevel() << "|r";
		}

		player->ADD_GOSSIP_ITEM(0, GuildInfo1.str(), GOSSIP_SENDER_MAIN, 0);
		player->ADD_GOSSIP_ITEM(0, GuildInfo2.str(), GOSSIP_SENDER_MAIN, 0);
	}

}

void LoadGuildPerkCommand(Player* player)
{
	Guild* guild = player->GetSession()->GetPlayer()->GetGuild();

	if (player->GetGuild())
	{
		if (guild->GetLevel() > 0)
		{
			if (guild->HasLevelForBonus(GUILD_BONUS_GOLD_1) && !guild->HasLevelForBonus(GUILD_BONUS_GOLD_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Gold bonus [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_XP_1) && !guild->HasLevelForBonus(GUILD_BONUS_XP_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Experience [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_SCHNELLER_GEIST))
				ChatHandler(player->GetSession()).PSendSysMessage("Faster Ghost");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_1) && !guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Cheaper Repairs [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_GOLD_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Gold bonus [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REITTEMPO_1) && !guild->HasLevelForBonus(GUILD_BONUS_RUF_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Mount Speed [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_RUF_1) && !guild->HasLevelForBonus(GUILD_BONUS_RUF_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Reputation [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_XP_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Experience [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Cheaper Repairs [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REITTEMPO_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Mount Speed [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Reputation [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_EHRE_1) && !guild->HasLevelForBonus(GUILD_BONUS_EHRE_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Honor [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_EHRE_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Honor [Rank 2]");
		}
		else
			ChatHandler(player->GetSession()).PSendSysMessage("None");
	}
}

class GuildInfo_NPC : public CreatureScript
{
public:
	GuildInfo_NPC() : CreatureScript("GuildInfo_NPC"){}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		// List Participant
		if (player->GetGuild())
		{
			LoadGuildLevelInfo(player);
			player->ADD_GOSSIP_ITEM(5, "Active Bonuses", GOSSIP_SENDER_MAIN, 1);
		}
		else
		{
			player->GetSession()->SendNotification("You're not member of a Guild", LANG_UNIVERSAL);
			player->CLOSE_GOSSIP_MENU();
		}
		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;

	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /* Sender */, uint32 Action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (Action)
		{
		case 0:
			OnGossipHello(player, creature);
			break;

		case 1:
			LoadGuildPerkCommand(player);
			player->CLOSE_GOSSIP_MENU();
			break;
		}

		return true;
	}
};

void AddSC_GuildInfo_NPC()
{
	new GuildInfo_NPC();
}